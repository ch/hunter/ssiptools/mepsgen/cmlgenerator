.. cmlgenerator documentation master file, created by
   sphinx-quickstart on Mon Dec 30 18:14:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cmlgenerator's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README.md
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
