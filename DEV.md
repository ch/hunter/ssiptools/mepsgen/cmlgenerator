# CML generator

This module is designed to generate CML that is element and attribute namespace qualified, from either SMILES or a 3D structure file as input.
This can be used either in a script or run at the commmand line.

## Installation from source

The module can be installed in python 3.x.


### Setting up the python environment ###

Clone the repository:

    git clone git@gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/mepsgen/cmlgenerator.git
    cd cmlgenerator

Details of an anaconda environment is provided in the repository with the required dependencies for this package.

	conda create -y -n cmlgenerator

This creates an environment called 'cmlgenerator' which can be loaded using:

	conda activate cmlgenerator

Install the needed deps from conda
       
	conda install -y -c conda-forge pip lxml pytest xmlvalidator openbabel numpy rdkit

### Using pip to install module ###

To install in your newly created environment, using pip, run:

	pip install .

This installs it in your current python environment.


To run the associated unit tests:

	pytest cmlgenerator


### Generating documetnation using Sphinx

Documentation can be rendered using sphinx.

    conda install -c conda-forge sphinx recommonmark
    cd docs
    make html

The rendered HTML can then be found in the build sub folder.
In progress: display of this documentation in a project wiki.
