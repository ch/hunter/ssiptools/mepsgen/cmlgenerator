# CML generator

This module is designed to generate CML that is element and attribute namespace qualified, from either SMILES or a 3D structure file as input.
This can be used either in a script or run at the commmand line.

## Installation

The module can be installed in python 3.x.
The latest release is available on [conda-forge](https://anaconda.org/conda-forge/cmlgenerator) conda-forge
and can be installed with [conda](https://docs.conda.io/en/latest/) via the command:

	conda install -c conda-forge cmlgenerator


#### Ziggy environment setup. ####

Note that if you are on ziggy you need to first load anaconda:

	module load anaconda/python3/4.4.0

Then create an environment (based on python 3.6):

	conda create -y -n nwchempy36 python=3.6

This is due to NWChem compilation issues with anaconda v5.X

	source activate nwchempy36
	conda install -c conda-forge cmlgenerator

## Running the generator

When in the python environment where it is installed, it can be called on the command line using:
    
    python -m cmlgenerator -h

This should display the following help output:

```
usage: __main__.py [-h] {generate,convert} ...

CLI for cmlgenerator module. See submodule help for more information.

optional arguments:
  -h, --help          show this help message and exit

Subparsers:
  {generate,convert}
    generate          Generate attribute qualified CML from many file formats
    convert           Convert attribute unqualified CML to attribute qualified
                      CML.
```

The convert module is used for conversion of CML with attributes namespace unqualified
to CML with attributes namespace qualified.

Example help information is shown below for the CLI:

```
~$ python -m cmlgenerator convert -h
usage: __main__.py convert [-h] [-o OUT_DIR] cmlregex

Converison of CML with unqualified attributes to qualified attributes.

positional arguments:
  cmlregex              REGEX expression for CML file location.

optional arguments:
  -h, --help            show this help message and exit
  -o OUT_DIR, --out_dir OUT_DIR
                        Output directory location

```


The generate module is used to generate CML with attributes namespace qualified
from a variety of file types.

SMILES and various other 3D representations are suitable input. For a full
list of the accepted £D representations please consult the openbabel documentation.


```
~$ python -m cmlgenerator generate -h
usage: __main__.py generate [-h] [-f F | -s] [-b] [--confs CONFS] [-o OUT_DIR]
                            molecule [molecule ...]

Script to carry out transformation to generate CML with full namespacing.

This can operate in two modes:
    1) single use mode:
        takes either a single SMILES or 3D structure file at the command line
        and outputs 1 cml file.
    2) batch mode:
        takes list of 3D structure files or a file containing a list of SMILES
        (one perline) and performs conversion

positional arguments:
  molecule              This is the SMILES or filename

optional arguments:
  -h, --help            show this help message and exit
  -f F                  Specify files to be read, of the given type.
  -s, --smiles          Specify input is SMILES
  -b, --batch           Specify if multiple SMILES in file rather than single
                        on command line
  --confs CONFS         number of conformers to generate from SMILES
  -o OUT_DIR, --out_dir OUT_DIR
                        Output directory location

Example usages:

        # For a single SMILES, example of benzene
        python -m cmlgenerator generate -s 'c1ccccc1'

        # For multiple SMILES strings, each on a new line in a file called smiles.txt
        python -m cmlgenerator generate -s -b smiles.txt

        # For a single pregenerated mol2 file
        python -m cmlgenerator generate -f mol2 molecule.mol2

```

### Contribution Guidlines

If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

#### Who do I talk to?

Any queries please contact Mark Driver.

### License

&copy; Mark Driver,  Christopher Hunter, Teodor Nikolov at the University of Cambridge

This is released under an AGPLv3 license for academic use.
Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:

 

Cambridge Enterprise Ltd
University of Cambridge
Hauser Forum
3 Charles Babbage Rd
Cambridge CB3 0GT
United Kingdom
Tel: +44 (0)1223 760339
Email: software@enterprise.cam.ac.uk

